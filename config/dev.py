SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@localhost:5432/spectra"
SESSION_TYPE = 'filesystem'
SECRET_KEY = 'secret'
DEBUG = True
UPLOAD_FOLDER = './uploads'
