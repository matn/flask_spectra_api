from app import app, db
from app.models import Spectra
import flask.ext.restless as restless

manager = restless.APIManager(app, flask_sqlalchemy_db = db)
manager.create_api(Spectra, results_per_page=None)

if __name__ == '__main__':
    app.run()