import numpy as np
import pandas as pd

def computePlanckSpectrumUVTP(path_to_XYZbar):
    planckSpectrumDf = pd.DataFrame(columns=wavelength_str)
    T = 1000
    Tp = [1000]
    i = 0

    while T < 21000:
        printProgress(T, 20000)
        planckSpectrumDf.loc[i] = computePlanckSpectrum(T)
        T = T + 0.01 * T
        Tp.append(T)
        i = i + 1

    planckSpectrumDf['X'] = np.zeros(len(planckSpectrumDf))
    planckSpectrumDf['Y'] = np.zeros(len(planckSpectrumDf))
    planckSpectrumDf['Z'] = np.zeros(len(planckSpectrumDf))

    for prog, ix in enumerate(planckSpectrumDf.index):
        printProgress(prog, len(planckSpectrumDf) - 1)
        XYZ = computeXYZ(planckSpectrumDf.loc[ix][wavelength_str], path_to_XYZbar)
        planckSpectrumDf.loc[ix][['X','Y','Z']] = XYZ

    denom = planckSpectrumDf[XYZ_str].sum(axis=1)

    planckSpectrumDf['x'] = planckSpectrumDf['X'] / denom
    planckSpectrumDf['y'] = planckSpectrumDf['Y'] / denom
    planckSpectrumDf['z'] = planckSpectrumDf['Z'] / denom


    up, vp = computeUV(planckSpectrumDf)


    return up, vp, Tp

def computePlanckSpectrum(temperature):
    cst1 = 3.74183E-16
    cst2 = 1.4388E-2
    wavelength = np.linspace(380E-9, 780E-9, 401)
    return cst1 * wavelength**(-5) / (np.exp(cst2/(wavelength*temperature)) -1)