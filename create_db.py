from app import db
from app.constants import *
from app.models import Spectra, spectra_from_csv
from app.spectra_utils import computePlanckSpectrumUVTP
from random import randint
import pandas as pd

def init(path_to_spectra='', create_planck_spectra=False):
    db.create_all()
    if path_to_spectra:
        pass


if __name__ == '__main__':
    spectra_from_csv('./app/static/ressources/spectra.csv')