from app import db
from app import spectra_utils
from app.spectra_utils import computePlanckSpectrumUVTP
from random import random
from sqlalchemy.types import ARRAY
import numpy as np
import pandas as pd
from flask_restful import Resource

class Spectra(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    resolution = db.Column(db.Float)
    spd = db.Column(db.ARRAY(db.Float))
    x = db.Column(db.Float)
    y = db.Column(db.Float)
    z = db.Column(db.Float)
    X = db.Column(db.Float)
    Y = db.Column(db.Float)
    Z = db.Column(db.Float)
    DUV = db.Column(db.Float)
    cct = db.Column(db.Float)
    tag = db.Column(db.ARRAY(db.String))

    def __init__(self, spd, X, Y, Z, x, y, z, U, V, DUV, cct,
                 min_=380, max_=780, resolution=1, tags = []):
        self.spd = spd
        self.X = X
        self.Y = Y
        self.Z = Z
        self.x = x
        self.y = y
        self.z = z
        self.U = U
        self.V = V
        self.DUV = DUV
        self.cct = cct
        self.resolution = resolution
        self.tag = tags

    def __repr__(self):
        return '<Spectrum  %r>' %(self.spd)

def get_spectra(cct=0):
    q = Spectra.query.filter()
    if cct:
        print "cct"
    return pd.read_sql_query(q.statement, q.session.bind, index_col='id').to_json()

def get_spds(cct=0):
    q = Spectra.query.filter()
    df = pd.read_sql_query(q.statement, q.session.bind, index_col='id')
    return df['spd'].to_json()

def spectra_from_dataframe(dataframe, min_=380, max_=780, resolution=1, tags=[]):
    XYZbar = np.genfromtxt('./app/static/ressources/xyz_bar.csv', delimiter=';')
    up, vp, Tp = computePlanckSpectrumUVTP(XYZbar)
    wavelength_range = [str(i) for i in range(min_,max_+1)]


    for i in dataframe.index:
        spd = dataframe.loc[i][wavelength_range]
        X,Y,Z = [np.float(x) for x in spectra_utils.computeXYZ(spd, XYZbar)]
        x,y,z = np.array([X,Y,Z])/np.array([X,Y,Z]).sum()
        U, V = spectra_utils.computeUV(x, y)
        DUV = spectra_utils.computeDUV(U,V)
        if DUV < 0.002 :
            cct = spectra_utils.computeTemperatureTriangular(U, V, up, vp, Tp)
        else:
            cct = spectra_utils.computeTemperatureParabolic(U, V, up, vp, Tp)

        s = Spectra(list(spd), X, Y, Z, x, y, z, U, V , DUV, cct,
                    min_, max_, resolution, tags)
        db.session.add(s)

    db.session.commit()