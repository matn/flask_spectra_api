from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pandas as pd

app = Flask("app")
app.config.from_envvar('CONFIG')
db = SQLAlchemy(app)


from app import views

