import pandas as pd
import numpy as np
from os import path

PATH_TO_XYZ_BAR = path.join(path.abspath(path.dirname(__file__)),
                            'static',
                            'ressources',
                            'xyz_bar.csv'
                            )

XYZ_BAR = np.genfromtxt(PATH_TO_XYZ_BAR, delimiter=";")
XYZ = list('XYZ')
TRICHROMATIC = list('xyz')

def allowed_file(filename):
    ALLOWED_EXTENSIONS = ['csv', 'md']
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def compute_xyz(spectrum_df):
    spectrum_df['x']

def compute_XYZ(spd):
    return

def compute_cct(spd):
    return

if __name__ == '__main__':
    print XYZ_BAR