from app import app
from app.models import *
from flask import render_template, send_from_directory, request, flash, redirect, url_for
from werkzeug.utils import secure_filename
from utils import allowed_file
import pandas as pd
import os

@app.route('/')
def index():
    args = request.args.get('test')
    app.logger.info(args)
    return render_template("plot.html")

@app.route('/add', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file_ = request.files['file']
        tags = request.values['tags'].split(';')
        # if user does not select file, browser also
        # submit a empty part without filename
        if file_.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if not allowed_file(file_.filename):
            flash('File type not supported')
            return redirect(request.url)

        if file_:
            spectra_from_dataframe(pd.read_csv(file_, delimiter=";"), tags=tags)

    return render_template('add.html')

@app.route('/uploads/<path:filename>')
def uploaded_file(filename):
    app.logger.info(app.config['UPLOAD_FOLDER'])
    return send_from_directory(
        os.path.abspath(app.config['UPLOAD_FOLDER']),
        filename
    )

@app.route('/spectra')
def load_spectra():
    args = dict(request.args)
    print get_spectra(**args)
    return 'test'


@app.route('/spectra/spds')
def load_spds():
    args = dict(request.args)
    return str(get_spds(**args))