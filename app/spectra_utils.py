# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from shapely.geometry import Point, Polygon

wavelength_str = [str(wl) for wl in range(380, 781)]
XYZ_str = ['X', 'Y', 'Z']
trichromatic = ['x', 'y', 'z']


def getSpectra(path, separator, index_col, wavelength_range, step_size, **kwargs):

    start = wavelength_range[0]
    stop = wavelength_range[1]
    nb = (stop - start) / step_size + 1
    wavelengths =  [str(int(wl)) for wl in np.linspace(start,stop,nb)]

    spectra = pd.read_csv(
        path,
        sep = separator,
        index_col = index_col
    )

    columns = wavelengths
    if set(XYZ_str).issubset(spectra.columns):
        columns = columns + XYZ_str

    if set(trichromatic).issubset(spectra.columns):
        columns = columns + trichromatic

    if set(['u', 'v']).issubset(spectra.columns):
        columns = columns + ['u', 'v']

    if 'cct' in spectra.columns:
        columns = columns + ['cct']

    if 'duv' in spectra.columns:
        columns = columns + ['duv']

    spectra = spectra[columns]


    return spectra, wavelengths

def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(barLength * iteration // total)
    bar = fill * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percent, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()

def inQuadrangles(point):
    quadrangles = cieQuadrangles()
    p = Point(point)
    for ix, quad in enumerate(quadrangles):
        poly = Polygon(quad)
        if poly.contains(p):
            return ix + 1

    return 0

def distance(a, b, norm=2):
    return np.power(np.sum(np.power(b-a,norm)), 1./norm)

def closeness(a,b, norm=2):
    return np.power(10, -distance(a,b,norm))

def testQuadrangles():
    quadrangles = cieQuadrangles()
    fig= plt.figure(figsize=(10,8))
    plt.plot([quadrangles[0,0,0], quadrangles[0,3,0]], [quadrangles[0,0,1], quadrangles[0,3,1]], 'k',lw=0.5)
    for i in range(8):
        plt.plot(quadrangles[i,:,0], quadrangles[i,:,1], 'k',lw=0.5)


    poly = Polygon(quadrangles[0])
    point = Point(quadrangles[0][0]-0.001)
    plt.plot(point.xy[0], point.xy[1], 'ro')

    print poly
    print Polygon(quadrangles[0]).contains(point)

    plt.show()


def cieQuadrangles():
    return np.array([
        [
            [0.48106,0.43146],
            [0.45614,0.42568],
            [0.43725,0.38922],
            [0.45906,0.39406]
        ],
        [
            [0.45614,0.42568],
            [0.43020,0.41713],
            [0.41493,0.38195],
            [0.43725,0.38922],

        ],
        [
            [0.43020,0.41713],
            [0.40026,0.40341],
            [0.38950,0.37075],
            [0.41493,0.38195]
        ],
        [
            [0.40026,0.40341],
            [0.37369,0.38794],
            [0.36712,0.35826],
            [0.38950,0.37075]
        ],
        [
            [0.37369,0.38794],
            [0.35500,0.37516],
            [0.35137,0.34797],
            [0.36712,0.35826]
        ],
        [
            [0.35500,0.37516],
            [0.33751,0.36185],
            [0.33660,0.33718],
            [0.35137,0.34797],
        ],
        [
            [0.33751,0.36185],
            [0.32052,0.34750],
            [0.32210,0.32547],
            [0.33660,0.33718],
        ],
        [
            [0.32052,0.34750],
            [0.30267,0.33095],
            [0.30670,0.31183],
            [0.32210,0.32547]
        ]
    ])

def computePlanckSpectrumUVTP(XYZ_bar):
    planckSpectrumDf = pd.DataFrame(columns=wavelength_str)
    T = 1000
    Tp = [1000]
    i = 0

    while T < 21000:
        planckSpectrumDf.loc[i] = computePlanckSpectrum(T)
        T = T + 0.01 * T
        Tp.append(T)
        i = i + 1

    planckSpectrumDf['X'] = np.zeros(len(planckSpectrumDf))
    planckSpectrumDf['Y'] = np.zeros(len(planckSpectrumDf))
    planckSpectrumDf['Z'] = np.zeros(len(planckSpectrumDf))

    for prog, ix in enumerate(planckSpectrumDf.index):
        XYZ = computeXYZ(planckSpectrumDf.loc[ix][wavelength_str], XYZ_bar)
        planckSpectrumDf.loc[ix][['X','Y','Z']] = [XYZ[i] for i in range(3)]

    denom = planckSpectrumDf[XYZ_str].sum(axis=1)

    planckSpectrumDf['x'] = planckSpectrumDf['X'] / denom
    planckSpectrumDf['y'] = planckSpectrumDf['Y'] / denom
    planckSpectrumDf['z'] = planckSpectrumDf['Z'] / denom


    up, vp = computeUV(planckSpectrumDf['x'], planckSpectrumDf['y'])
    return up, vp, Tp

def computePlanckSpectrum(temperature):
    cst1 = 3.74183E-16
    cst2 = 1.4388E-2
    wavelength = np.linspace(380E-9, 780E-9, 401)
    return cst1 * wavelength**(-5) / (np.exp(cst2/(wavelength*temperature)) -1)

def computeXYZ(spectrumDF, XYZbar):
    return 683*XYZbar * np.transpose(np.matrix(spectrumDF))

def computeUV(x,y):
    denom = (-2 * x + 12 * y + 3)

    u = 4 * x / denom
    v = 6 * y / denom

    return u, v

def computeTemperatureTriangular(u, v, up, vp, Tp):
    t2 = len(up)
    print u - up
    D = np.sqrt((u - up) ** 2 + (v - vp) ** 2)
    m = np.argmin(D)
    l = np.sqrt((up[m+1] - up[m-1]) ** 2 + (vp[m+1] - vp[m-1]) ** 2)
    x = (D[m-1] ** 2 + D[m+1] ** 2) / (2*l)
    Tx = Tp[m - 1] + (Tp[m + 1] - Tp[m-1])*x /l
    Txcorr = Tx * 0.99991


    return Txcorr

def computeTemperatureParabolic(u, v, up, vp, Tp):

    D = np.sqrt((u - up) ** 2 + (v - vp) ** 2)
    m = np.argmin(D)
    if m == len(D) - 1:
        m = m - 1

    X = (Tp[m+1] - Tp[m]) * (Tp[m-1] - Tp[m+1]) * (Tp[m]-Tp[m+1])
    a = (
            Tp[m-1] * ( D[m+1] - D[m]) + \
            Tp[m]   * ( D[m-1] - D[m+1]) + \
            Tp[m+1] * ( D[m]   - D[m-1])
        )/X;

    b = -(
            Tp[m-1]**2 * (D[m+1]-D[m])+ \
            Tp[m]**2 * (D[m-1]-D[m+1])+ \
            Tp[m+1]**2 * (D[m]-D[m-1]) \
        )/X;

    c = -(
            D[m-1]*(Tp[m+1]-Tp[m])*Tp[m]*Tp[m+1]+ \
            D[m]*(Tp[m-1]-Tp[m+1])*Tp[m-1]*Tp[m+1]+\
            D[m+1]*(Tp[m]-Tp[m-1])*Tp[m-1]*Tp[m]
         )/X;

    Tx=-b/(2*a);
    Txcorr=Tx*0.99991;

    return Txcorr


def computeDUV(u, v):

    Lfp = np.sqrt((u - 0.292)**2 + (v - 0.240)**2)
    a = np.arccos((u - 0.292) / Lfp)
    Lbb = -0.00616793*a ** 6 + \
            0.0893944*a ** 5 + \
            -0.5179722*a ** 4+ \
            1.5317403*a ** 3 - \
            2.4243787*a ** 2 + \
            1.925865*a -  \
            0.471106

    return Lfp + Lbb

def smoothSpectrum(spectrumDF, window, polyorder):
    from scipy.signal import savgol_filter

    spectrumDF = savgol_filter(
        spectrumDF,
        window,
        polyorder
    )

def smoothSpectra(spectraDF, window, polyorder):
    for i in spectraDF.index:
        print i
        smoothSpectrum(spectraDF.ix[i], window, polyorder)


def extrapolateSpectrum(spectrumDF, extrapolationRange, interpolationRange, wavelengths):
    from scipy import stats
    range_extrapolation_str = [str(i) for i in extrapolationRange]
    range_interpolation_str = [str(i) for i in interpolationRange]
    X = np.log(spectrumDF)
    slope, intercept, r_value, p_value, std_err = stats.linregress(
        np.array(interpolationRange),
        X.loc[range_interpolation_str]
    )

    print slope, intercept
    X_extra = np.array(extrapolationRange) * slope + intercept
    spectrumDF[range_extrapolation_str] = np.exp(X_extra)/(1+np.exp(X_extra))

def extrapolateSpectra(spectraDF, extrapolationRange, interpolationRange, wavelengths):
    for i in spectraDF.index:
        print i
        extrapolateSpectrum(spectraDF.ix[i], extrapolationRange, interpolationRange, wavelengths)

if __name__ == '__main__':
    data_config = {
      "path": "../projet/representative.csv",
      "separator": ";",
      "index_col": "id",
      "wavelength_range":[380,780],
      "step_size":1
    }
    spectra, wl = getSpectra(**data_config)
    print type(spectra.loc[1][0])
    extrapolationRange = range(380, 411)
    interpolationRange = range(410, 420)

    window = 21
    polyorder = 3
    smoothSpectra(spectra, window, polyorder)
    extrapolateSpectra(spectra, extrapolationRange, interpolationRange, wl)
